# event_driven_design_eshop

## Project Description

This project is a practical implementation of distributed Spring Boot microservices. It focuses on building a system with business logic that spans several distributed microservices. The project is designed to showcase key concepts such as microservices architecture, Spring Boot, Spring Cloud, distributed transactions, event-based microservices using Axon Framework, and the Saga design pattern.

## Features

- **Microservices Architecture**: The project follows a microservices architecture, allowing for the development of independently deployable services.

- **API Gateway**: Implement an API Gateway to manage multiple instances of microservices and make them discoverable.

- **Scaling**: Scale microservices up and down as needed to handle varying workloads.

- **Distributed Transactions**: Working with distributed transactions and handling error scenarios.

- **Event-based Microservices**: Develop event-based microservices using the Axon Framework, following the CQRS principle.

- **Event Sourcing**: Implementing CQRS and Event Sourcing in microservices.

- **Saga Design Pattern**: Grouping multiple operations into a single transaction using the Saga design pattern.

## Requirements

* Java 17
* Docker

## Build and run

[//]: # (```shell)

[//]: # (```)

## Overview

This project was developed with a focus on exploring event-driven design concepts, even in situations where they may not be strictly necessary for the primary use case. One such example is the implementation of a saga within the application.

## The Saga

In this project, I intentionally included a saga, even though the use case could have been solved with a simpler event-handling component in the order-service. The decision to use a saga was driven by my desire to gain hands-on experience with event-driven design patterns and concepts.