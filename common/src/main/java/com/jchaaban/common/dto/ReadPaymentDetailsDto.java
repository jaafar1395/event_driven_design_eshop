package com.jchaaban.common.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReadPaymentDetailsDto {

    private final String paymentDetailsId;

    private final String userId;

    private final String cardNumber;

    private final int validUntilMonth;

    private final int validUntilYear;

    private final String cvv;

    private final BigDecimal balance;

}
