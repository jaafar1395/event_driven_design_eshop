package com.jchaaban.common.dto;

import lombok.Data;

import java.util.Map;

@Data
public class ReadProductsPricesDto {
    private final Map<String, Double> productsPrices;
}
