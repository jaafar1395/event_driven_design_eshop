package com.jchaaban.common.dto;

import lombok.Data;

@Data
public class ReadUserAddressDto {
    private final String addressId;
    private final String streetAddress;
    private final String city;
    private final String state;
    private final String postalCode;
    private final String country;
}
