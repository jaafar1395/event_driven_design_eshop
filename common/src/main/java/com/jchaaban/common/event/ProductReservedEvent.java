package com.jchaaban.common.event;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ProductReservedEvent {
    private final String productId;
    private final String orderId;
    private final int quantity;
    private final BigDecimal price;
}
