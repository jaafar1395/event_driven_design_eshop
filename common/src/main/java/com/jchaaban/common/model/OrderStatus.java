package com.jchaaban.common.model;

public enum OrderStatus {
    CREATED, APPROVED, REJECTED, DELETED, SHIPPED

}
