package com.jchaaban.common.query;

import lombok.Data;

@Data
public class FetchOrderQuery {
    private final String orderId;
}
