package com.jchaaban.common.query;

import lombok.Data;

@Data
public class FetchUserPaymentDetailsQuery {
    private final String userId;
}
