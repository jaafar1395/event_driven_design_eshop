package com.jchaaban.ordersservice.command;

import com.jchaaban.common.model.OrderStatus;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

import java.math.BigDecimal;

@Data
public class ApproveOrderCommand {

    @TargetAggregateIdentifier
    private final String orderId;
    private final OrderStatus orderStatus;
    private final BigDecimal orderTotalPrice;
}
