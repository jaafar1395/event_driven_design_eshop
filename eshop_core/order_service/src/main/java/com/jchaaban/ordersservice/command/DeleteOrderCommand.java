package com.jchaaban.ordersservice.command;


import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
public class DeleteOrderCommand {

    @TargetAggregateIdentifier
    private final String orderId;
}
