package com.jchaaban.ordersservice.event;

import lombok.Data;

@Data
public class OrderRejectedEvent {
    private final String orderId;
    private final String reason;
}
