package com.jchaaban.ordersservice.handler.query;

import com.jchaaban.common.query.FetchOrderQuery;
import com.jchaaban.ordersservice.query.FetchOrdersQuery;
import com.jchaaban.ordersservice.repository.OrderEntity;
import com.jchaaban.ordersservice.repository.OrderRepository;
import com.jchaaban.ordersservice.rest.dto.ReadOrderDto;
import com.jchaaban.ordersservice.rest.exception.OrderNotFoundException;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class OrderFetchingHandler {

    private final OrderRepository orderRepository;

    public OrderFetchingHandler(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @QueryHandler
    public ReadOrderDto fetchOrder(FetchOrderQuery fetchOrderQuery){
        String orderId = fetchOrderQuery.getOrderId();
        Optional<OrderEntity> orderEntityOptional = orderRepository.findById(orderId);
        if (orderEntityOptional.isEmpty()) {
            throw new OrderNotFoundException(orderId);
        }

        OrderEntity orderEntity = orderEntityOptional.get();
        return new ReadOrderDto(
                orderEntity.getOrderId(),
                orderEntity.getUserId(),
                orderEntity.getProductIds(),
                orderEntity.getOrderStatus(),
                orderEntity.getTotalPrice()
        );
    }
    @QueryHandler
    public List<ReadOrderDto> fetchOrders(FetchOrdersQuery fetchOrdersQuery) {
        List<OrderEntity> orderEntities = orderRepository.findAll();

        return orderEntities.stream()
                .map(orderEntity -> new ReadOrderDto(
                        orderEntity.getOrderId(),
                        orderEntity.getUserId(),
                        orderEntity.getProductIds(),
                        orderEntity.getOrderStatus(),
                        orderEntity.getTotalPrice()
                ))
                .collect(Collectors.toList());
    }
}
