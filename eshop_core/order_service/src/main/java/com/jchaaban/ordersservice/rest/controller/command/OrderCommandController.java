package com.jchaaban.ordersservice.rest.controller.command;

import com.jchaaban.common.model.OrderStatus;
import com.jchaaban.common.query.FetchOrderQuery;
import com.jchaaban.ordersservice.command.CreateOrderCommand;
import com.jchaaban.ordersservice.command.DeleteOrderCommand;
import com.jchaaban.ordersservice.rest.dto.CreateOrderDto;
import com.jchaaban.ordersservice.rest.dto.OrderSummary;
import com.jchaaban.ordersservice.rest.exception.OrderNotFoundException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.queryhandling.SubscriptionQueryResult;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
@RestController
@RequestMapping("/orders")
public class OrderCommandController {
    private final CommandGateway commandGateway;
    private final QueryGateway queryGateway;
    public OrderCommandController(CommandGateway commandGateway, QueryGateway queryGateway) {
        this.commandGateway = commandGateway;
        this.queryGateway = queryGateway;
    }

    @PostMapping
    public ResponseEntity<OrderSummary> createOrder(@RequestBody CreateOrderDto createOrderDto) {
        CreateOrderCommand createOrderCommand = new CreateOrderCommand(
                UUID.randomUUID().toString(),
                createOrderDto.getUserId(),
                createOrderDto.getProductId(),
                OrderStatus.CREATED
        );

        try (SubscriptionQueryResult<OrderSummary, OrderSummary> queryResult = queryGateway.subscriptionQuery(
                new FetchOrderQuery(createOrderCommand.getOrderId()),
                ResponseTypes.instanceOf(OrderSummary.class),
                ResponseTypes.instanceOf(OrderSummary.class)
        )) {
            commandGateway.sendAndWait(createOrderCommand);
            return new ResponseEntity<>(
                    queryResult.updates().blockFirst(), HttpStatus.OK
            );
        }
    }


    @DeleteMapping("/{orderId}")
    public ResponseEntity<String> deleteOrder(@PathVariable String orderId) {

        DeleteOrderCommand deleteOrderCommand = new DeleteOrderCommand(orderId);

        commandGateway.sendAndWait(deleteOrderCommand);

        return new ResponseEntity<>(
                "Order was successfully deleted", HttpStatus.OK
        );
    }

    @ExceptionHandler(OrderNotFoundException.class)
    public ResponseEntity<String> handleOrderNotFoundException(
            OrderNotFoundException orderNotFoundException
    ) {
        return new ResponseEntity<>(orderNotFoundException.getMessage(), HttpStatus.BAD_REQUEST);
    }


}
