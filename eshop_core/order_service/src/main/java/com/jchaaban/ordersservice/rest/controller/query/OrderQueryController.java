package com.jchaaban.ordersservice.rest.controller.query;

import com.jchaaban.common.query.FetchOrderQuery;
import com.jchaaban.ordersservice.query.FetchOrdersQuery;
import com.jchaaban.ordersservice.rest.dto.ReadOrderDto;
import com.jchaaban.ordersservice.rest.exception.OrderServiceErrorResponse;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryExecutionException;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderQueryController {
    private final QueryGateway queryGateway;

    public OrderQueryController(QueryGateway queryGateway) {
        this.queryGateway = queryGateway;
    }

    @GetMapping
    public List<ReadOrderDto> getOrders() {
        FetchOrdersQuery orderQuery = new FetchOrdersQuery();
        return queryGateway.query(
                orderQuery,
                ResponseTypes.multipleInstancesOf(ReadOrderDto.class)
        ).join();
    }

    @GetMapping("/{orderId}")
    public ReadOrderDto getOrder(@PathVariable String orderId) {
        FetchOrderQuery orderQuery = new FetchOrderQuery(orderId);
        return queryGateway.query(
                orderQuery,
                ResponseTypes.instanceOf(ReadOrderDto.class)
        ).join();
    }

    @ExceptionHandler(QueryExecutionException.class)
    public ResponseEntity<OrderServiceErrorResponse> handleQueryExecution(
            QueryExecutionException ex
    ) {
        String errorMessage = ex.getMessage();
        OrderServiceErrorResponse errorResponse = new OrderServiceErrorResponse(errorMessage);

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

}
