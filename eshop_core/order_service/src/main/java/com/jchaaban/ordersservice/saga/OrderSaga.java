package com.jchaaban.ordersservice.saga;

import com.jchaaban.common.command.CreateShipmentCommand;
import com.jchaaban.common.command.PorcessPaymentCommand;
import com.jchaaban.common.command.ReserveProductCommand;
import com.jchaaban.common.command.UnreserveProductCommand;
import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import com.jchaaban.common.event.PaymentProcessedEvent;
import com.jchaaban.common.event.ProductReservedEvent;
import com.jchaaban.common.event.ShipmentCreatedEvent;
import com.jchaaban.common.model.Address;
import com.jchaaban.common.model.OrderStatus;
import com.jchaaban.common.model.ShippingStatus;
import com.jchaaban.common.query.CheckUserExistsQuery;
import com.jchaaban.common.query.FetchUserAddressQuery;
import com.jchaaban.common.query.FetchUserPaymentDetailsQuery;
import com.jchaaban.ordersservice.command.ApproveOrderCommand;
import com.jchaaban.ordersservice.command.RejectOrderCommand;
import com.jchaaban.ordersservice.event.OrderApprovedEvent;
import com.jchaaban.ordersservice.event.OrderCreatedEvent;
import com.jchaaban.ordersservice.saga.service.OrderSummaryEmitter;
import lombok.NoArgsConstructor;
import org.axonframework.commandhandling.CommandExecutionException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.modelling.saga.EndSaga;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.modelling.saga.StartSaga;
import org.axonframework.queryhandling.QueryGateway;
import org.axonframework.spring.stereotype.Saga;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Saga
@NoArgsConstructor
public class OrderSaga {
    public static final String PRODUCT_RESERVATION_FAILED = "Reservation failed for Product having ID: %s";
    private static final String USER_NOT_FOUND_TEMPLATE = "User with ID: %s not found.";
    private static final String PAYMENT_DETAILS_NOT_FOUND = "Payment details for user with ID: %s not found.";
    private static final String ORDER_SUCCESS_TEMPLATE = "Order processed successfully. Total cost: %s$ including shipping cost";
    public static final int SHIPPING_COST = 20;
    private static final Logger logger = Logger.getLogger(OrderSaga.class.getName());

    @Autowired
    private transient CommandGateway commandGateway;

    @Autowired
    private transient QueryGateway queryGateway;

    @Autowired
    private transient OrderSummaryEmitter orderSummaryEmitter;
    private String orderId;
    private String userId;
    private Map<String, Integer> productsToReserve;
    private Map<String, Integer> reservedProducts;
    private int productsToReserveCount;
    private BigDecimal totalPrice;
    private BigDecimal shippingCost;

    @StartSaga
    @SagaEventHandler(associationProperty = "orderId")
    public void handle(OrderCreatedEvent event) {
        logger.info("Handling OrderCreatedEvent in Saga for orderId: " + event.getOrderId());

        initializeSagaStateFields(event);

        if (!userExists()) {
            rejectOrder(String.format(USER_NOT_FOUND_TEMPLATE, userId));
            return;
        }

        reserveProducts();
    }


    @SagaEventHandler(associationProperty = "orderId")
    public void handle(ProductReservedEvent productReservedEvent) {
        logger.info("Handling ProductReservedEvent in Saga");

        updateTotalPrice(productReservedEvent.getPrice(), productReservedEvent.getQuantity());
        reservedProducts.put(productReservedEvent.getProductId(), productReservedEvent.getQuantity());

        if (productsToReserveCount > 1) {
            --productsToReserveCount;
            return;
        }

        sendProcessPaymentCommand();
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(PaymentProcessedEvent paymentProcessedEvent) {
        logger.info("we are about to do PaymentProcessedEvent " + totalPrice);

        if (productsToReserveCount > 1) {
            return;
        }

        commandGateway.sendAndWait(new ApproveOrderCommand(
                paymentProcessedEvent.getOrderId(),
                OrderStatus.APPROVED,
                totalPrice.add(shippingCost)
        ));
    }

    @SagaEventHandler(associationProperty = "orderId")
    public void handle(OrderApprovedEvent orderApprovedEvent) {
        logger.info("we are about to do OrderApprovedEvent " + totalPrice);

        Address userAddress = fetchUserAddress();

        CreateShipmentCommand createShipmentCommand = new CreateShipmentCommand(
                UUID.randomUUID().toString(),
                orderId,
                userAddress,
                ShippingStatus.DELIVERED,
                UUID.randomUUID().toString(),
                shippingCost,
                totalPrice.add(shippingCost)
        );

        commandGateway.sendAndWait(createShipmentCommand);
    }


    @EndSaga
    @SagaEventHandler(associationProperty = "orderId")
    public void handle(ShipmentCreatedEvent shipmentCreatedEvent) {
        logger.info("Handling ShipmentCreatedEvent in Saga");

        if (productsToReserveCount > 1) {
            return;
        }
        orderSummaryEmitter.emitOrderSummary(
                shipmentCreatedEvent.getOrderId(),
                String.format(ORDER_SUCCESS_TEMPLATE, totalPrice.add(new BigDecimal(SHIPPING_COST)))
        );
    }

    private void initializeSagaStateFields(OrderCreatedEvent event) {
        orderId = event.getOrderId();
        userId = event.getUserId();
        totalPrice = new BigDecimal(0);
        reservedProducts = new HashMap<>();
        productsToReserve = countProductOccurrences(event.getProductIds());
        productsToReserveCount = productsToReserve.keySet().size();
        shippingCost = new BigDecimal(SHIPPING_COST);
    }

    private boolean userExists() {
        return queryGateway.query(new CheckUserExistsQuery(userId), Boolean.class)
                .exceptionally(ex -> null)
                .join();
    }
    private void reserveProducts() {
        productsToReserve.forEach((productId, quantity) -> {
            commandGateway.send(
                    new ReserveProductCommand(productId, orderId,  quantity),
                    (commandMessage, commandResultMessage) -> {
                        if (commandResultMessage.isExceptional()) {
                            rejectOrder(String.format(PRODUCT_RESERVATION_FAILED, productId));
                        }
                    }
            );
            reservedProducts.put(productId, quantity);
        });
    }

    private void sendProcessPaymentCommand() {
        ReadPaymentDetailsDto paymentDetails = fetchUserPaymentDetails();

        if (paymentDetails == null) {
            rejectOrder(String.format(PAYMENT_DETAILS_NOT_FOUND, userId));
            return;
        }

        try {
            commandGateway.sendAndWait(new PorcessPaymentCommand(
                    paymentDetails.getPaymentDetailsId(),
                    orderId,
                    totalPrice.add(shippingCost)
            ));

        } catch (CommandExecutionException commandExecutionException) {
            rejectOrder(commandExecutionException.getMessage());
        }
    }

    private ReadPaymentDetailsDto fetchUserPaymentDetails() {
        return queryGateway.query(new FetchUserPaymentDetailsQuery(userId), ReadPaymentDetailsDto.class)
                .exceptionally(ex -> null)
                .join();
    }

    private void rejectOrder(String reason) {
        logger.info("Rejecting order");
        commandGateway.send(new RejectOrderCommand(orderId, reason));
        rollbackProductsReservation();
        orderSummaryEmitter.emitOrderSummary(orderId, reason);
    }
    private Address fetchUserAddress() {
        return queryGateway.query(new FetchUserAddressQuery(userId), ResponseTypes.instanceOf(Address.class))
                .exceptionally(ex -> null)
                .join();
    }

    private void rollbackProductsReservation() {
        logger.info("Rolling back products reservations");
        reservedProducts.forEach((productId, quantity) ->
                commandGateway.send(new UnreserveProductCommand(productId, orderId, quantity))
        );
    }

    private void updateTotalPrice(BigDecimal price, int quantity) {
        totalPrice = totalPrice.add(price.multiply(new BigDecimal(quantity)));
    }

    private Map<String, Integer> countProductOccurrences(List<String> productIds) {
        return productIds.stream()
                .collect(Collectors.groupingBy(productId -> productId, Collectors.summingInt(productId -> 1)));
    }
}