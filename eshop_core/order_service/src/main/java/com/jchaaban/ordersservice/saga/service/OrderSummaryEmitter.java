package com.jchaaban.ordersservice.saga.service;

import com.jchaaban.common.query.FetchOrderQuery;
import com.jchaaban.ordersservice.rest.dto.OrderSummary;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.stereotype.Service;

@Service
public class OrderSummaryEmitter {

    private final transient QueryUpdateEmitter queryUpdateEmitter;

    public OrderSummaryEmitter(QueryUpdateEmitter queryUpdateEmitter) {
        this.queryUpdateEmitter = queryUpdateEmitter;
    }

    public void emitOrderSummary(String orderId, String message) {
        OrderSummary summary = new OrderSummary(orderId, message);
        queryUpdateEmitter.emit(FetchOrderQuery.class, query -> true, summary);
    }
}