package com.jchaaban.paymentservice.handler.query;

import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import com.jchaaban.common.query.FetchUserPaymentDetailsQuery;
import com.jchaaban.paymentservice.query.FetchAllPaymentDetailsQuery;
import com.jchaaban.paymentservice.query.FetchPaymentDetailsQuery;
import com.jchaaban.paymentservice.repository.PaymentDetailsEntity;
import com.jchaaban.paymentservice.repository.PaymentDetailsRepository;
import com.jchaaban.paymentservice.rest.exception.PaymentDetailsNotFoundException;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PaymentFetchingQueryHandler {

    private final PaymentDetailsRepository paymentDetailsRepository;

    public PaymentFetchingQueryHandler(PaymentDetailsRepository productRepository) {
        this.paymentDetailsRepository = productRepository;
    }

    @QueryHandler
    public List<ReadPaymentDetailsDto> handle(FetchAllPaymentDetailsQuery fetchAllPaymentDetailsQuery){
        ArrayList<ReadPaymentDetailsDto> paymentDetailsDtos = new ArrayList<>();
        List<PaymentDetailsEntity> storedPaymentDetails = paymentDetailsRepository.findAll();

        storedPaymentDetails.forEach(
                paymentDetail -> paymentDetailsDtos.add(new ReadPaymentDetailsDto(
                        paymentDetail.getPaymentDetailsId(),
                        paymentDetail.getUserId(),
                        paymentDetail.getCardNumber(),
                        paymentDetail.getValidUntilMonth(),
                        paymentDetail.getValidUntilYear(),
                        paymentDetail.getCvv(),
                        paymentDetail.getBalance()
                ))
        );
        return paymentDetailsDtos;
    }


    @QueryHandler
    public ReadPaymentDetailsDto handle(FetchPaymentDetailsQuery fetchPaymentDetailsQuery){
        String paymentDetailsId = fetchPaymentDetailsQuery.getPaymentDetailsId();
        PaymentDetailsEntity paymentDetailsEntity = paymentDetailsRepository.findByPaymentDetailsId(paymentDetailsId);

        if (paymentDetailsEntity == null){
            throw new PaymentDetailsNotFoundException("Payment details having ID " + paymentDetailsId + " doesn't exist");
        }

        return new ReadPaymentDetailsDto(
                paymentDetailsId,
                paymentDetailsEntity.getUserId(),
                paymentDetailsEntity.getCardNumber(),
                paymentDetailsEntity.getValidUntilMonth(),
                paymentDetailsEntity.getValidUntilYear(),
                paymentDetailsEntity.getCvv(),
                paymentDetailsEntity.getBalance()
        );
    }

    @QueryHandler
    public ReadPaymentDetailsDto handle(FetchUserPaymentDetailsQuery fetchUserPaymentDetailsQuery){
        String userId = fetchUserPaymentDetailsQuery.getUserId();
        PaymentDetailsEntity paymentDetailsEntity = paymentDetailsRepository.findByUserId(userId);

        if (paymentDetailsEntity == null){
            throw new PaymentDetailsNotFoundException("No payment details found for the user having ID: " + userId);
        }

        return new ReadPaymentDetailsDto(
                paymentDetailsEntity.getPaymentDetailsId(),
                userId,
                paymentDetailsEntity.getCardNumber(),
                paymentDetailsEntity.getValidUntilMonth(),
                paymentDetailsEntity.getValidUntilYear(),
                paymentDetailsEntity.getCvv(),
                paymentDetailsEntity.getBalance()
        );
    }
}
