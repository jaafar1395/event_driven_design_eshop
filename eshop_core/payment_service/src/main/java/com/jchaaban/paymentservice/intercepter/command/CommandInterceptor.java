package com.jchaaban.paymentservice.intercepter.command;

import com.jchaaban.paymentservice.command.PaymentDetailsCommand;
import com.jchaaban.paymentservice.command.validator.PaymentDetailsCommandValidator;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.BiFunction;

@Component
public class CommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

    private final PaymentDetailsCommandValidator paymentDetailsCommandValidator;

    public CommandInterceptor(PaymentDetailsCommandValidator paymentDetailsCommandValidator) {
        this.paymentDetailsCommandValidator = paymentDetailsCommandValidator;
    }

    @Nonnull
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(
            @Nonnull List<? extends CommandMessage<?>> list
    ) {
        return (index, command) -> {
            paymentDetailsCommandValidator.validate((PaymentDetailsCommand) command.getPayload());
            return command;
        };
    }

}
