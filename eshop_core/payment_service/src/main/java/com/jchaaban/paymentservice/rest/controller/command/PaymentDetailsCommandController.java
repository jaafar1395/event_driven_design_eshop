package com.jchaaban.paymentservice.rest.controller.command;

import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import com.jchaaban.paymentservice.command.CreatePaymentDetailsCommand;
import com.jchaaban.paymentservice.command.DeletePaymentDetailsCommand;
import com.jchaaban.paymentservice.command.UpdatePaymentDetailsCommand;
import com.jchaaban.paymentservice.rest.dto.CreatePaymentDetailsDto;
import com.jchaaban.paymentservice.rest.dto.UpdatePaymentDetailsDto;
import com.jchaaban.paymentservice.rest.exception.DuplicateCardNumberException;
import com.jchaaban.paymentservice.rest.exception.PaymentDetailsInvalidArgumentsException;
import com.jchaaban.paymentservice.rest.exception.PaymentDetailsNotFoundException;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/payments")
public class PaymentDetailsCommandController {
    private final CommandGateway commandGateway;

    public PaymentDetailsCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping
    public ResponseEntity<ReadPaymentDetailsDto> createPaymentDetails(
            @RequestBody CreatePaymentDetailsDto createPaymentDetailsDto
    ) {
        String paymentDetailsId = UUID.randomUUID().toString();
        CreatePaymentDetailsCommand createPaymentDetailsCommand = new CreatePaymentDetailsCommand(
                paymentDetailsId,
                createPaymentDetailsDto.getUserId(),
                createPaymentDetailsDto.getCardNumber(),
                createPaymentDetailsDto.getValidUntilMonth(),
                createPaymentDetailsDto.getValidUntilYear(),
                createPaymentDetailsDto.getCvv(),
                createPaymentDetailsDto.getBalance());

        commandGateway.sendAndWait(createPaymentDetailsCommand);

        return new ResponseEntity<>(
                createPaymentDetailsCommand.toToReadProductDto(), HttpStatus.OK
        );
    }

    @PutMapping("/{paymentDetailsId}")
    public ResponseEntity<ReadPaymentDetailsDto> editPaymentDetails(
            @PathVariable String paymentDetailsId,
            @RequestBody UpdatePaymentDetailsDto updatePaymentDetailsDto
    ) {
        UpdatePaymentDetailsCommand updatePaymentDetailsCommand = new UpdatePaymentDetailsCommand(
                paymentDetailsId,
                updatePaymentDetailsDto.getUserId(),
                updatePaymentDetailsDto.getCardNumber(),
                updatePaymentDetailsDto.getValidUntilMonth(),
                updatePaymentDetailsDto.getValidUntilYear(),
                updatePaymentDetailsDto.getCvv(),
                updatePaymentDetailsDto.getBalance()
        );

        commandGateway.sendAndWait(updatePaymentDetailsCommand);

        return new ResponseEntity<>(
                updatePaymentDetailsCommand.toToReadProductDto(), HttpStatus.OK
        );
    }

    @DeleteMapping("/{paymentDetailsId}")
    public ResponseEntity<String> deletePaymentDetailsId(@PathVariable String paymentDetailsId) {

        DeletePaymentDetailsCommand deleteProductCommand = new DeletePaymentDetailsCommand(paymentDetailsId);

        commandGateway.sendAndWait(deleteProductCommand);

        return new ResponseEntity<>(
                "Payment details was successfully deleted", HttpStatus.OK
        );
    }

    @ExceptionHandler(PaymentDetailsInvalidArgumentsException.class)
    public ResponseEntity<String> handlePaymentDetailsInvalidArgumentsException(
            PaymentDetailsInvalidArgumentsException paymentDetailsInvalidArgumentsException
    ) {
        return new ResponseEntity<>(paymentDetailsInvalidArgumentsException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DuplicateCardNumberException.class)
    public ResponseEntity<String> handleDuplicateCardNumberException(
            DuplicateCardNumberException duplicateCardNumberException
    ) {
        return new ResponseEntity<>(duplicateCardNumberException.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(PaymentDetailsNotFoundException.class)
    public ResponseEntity<String> handlePaymentDetailsNotFoundException(
            PaymentDetailsNotFoundException paymentDetailsNotFoundException
    ) {
        return new ResponseEntity<>(paymentDetailsNotFoundException.getMessage(), HttpStatus.NOT_FOUND);
    }
}
