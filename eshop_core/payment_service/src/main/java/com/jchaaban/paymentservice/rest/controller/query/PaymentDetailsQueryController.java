package com.jchaaban.paymentservice.rest.controller.query;

import com.jchaaban.common.dto.ReadPaymentDetailsDto;
import com.jchaaban.paymentservice.query.FetchAllPaymentDetailsQuery;
import com.jchaaban.paymentservice.query.FetchPaymentDetailsQuery;
import com.jchaaban.paymentservice.rest.exception.PaymentServiceErrorResponse;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryExecutionException;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/payments")
public class PaymentDetailsQueryController {
    private final QueryGateway queryGateWay;

    public PaymentDetailsQueryController(QueryGateway queryGateWay) {
        this.queryGateWay = queryGateWay;
    }

    @GetMapping
    public List<ReadPaymentDetailsDto> getPaymentDetails() {

        FetchAllPaymentDetailsQuery fetchPaymentDetailsQuery = new FetchAllPaymentDetailsQuery();
        return queryGateWay.query(
                fetchPaymentDetailsQuery,
                ResponseTypes.multipleInstancesOf(ReadPaymentDetailsDto.class)
        ).join();
    }


    @GetMapping("/{paymentDetailsId}")
    public ReadPaymentDetailsDto deletePaymentDetailsId(@PathVariable String paymentDetailsId) {

        FetchPaymentDetailsQuery fetchPaymentDetailsQuery = new FetchPaymentDetailsQuery(
                paymentDetailsId
        );
        return queryGateWay.query(
                fetchPaymentDetailsQuery,
                ResponseTypes.instanceOf(ReadPaymentDetailsDto.class)
        ).join();
    }

    @ExceptionHandler(QueryExecutionException.class)
    public ResponseEntity<PaymentServiceErrorResponse> handleQueryExecution(
            QueryExecutionException queryExecutionException
    ) {
        String errorMessage = queryExecutionException.getMessage();
        PaymentServiceErrorResponse errorResponse = new PaymentServiceErrorResponse(errorMessage);

        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
