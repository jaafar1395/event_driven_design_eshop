package com.jchaaban.paymentservice.rest.exception;

public class PaymentDetailsNotFoundException extends PaymentServiceException {
    public PaymentDetailsNotFoundException(String message) {
        super(message);
    }
}
