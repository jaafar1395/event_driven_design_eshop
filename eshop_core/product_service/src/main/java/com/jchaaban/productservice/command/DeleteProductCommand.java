package com.jchaaban.productservice.command;

import lombok.Getter;

@Getter
public class DeleteProductCommand extends ProductCommand {
    public DeleteProductCommand(String productId) {
        super(productId);
    }
}
