package com.jchaaban.productservice.handler.query;

import com.jchaaban.productservice.query.FetchProductQuery;
import com.jchaaban.productservice.query.FetchProductsQuery;
import com.jchaaban.productservice.repository.ProductEntity;
import com.jchaaban.productservice.repository.ProductRepository;
import com.jchaaban.productservice.rest.dto.ReadProductDto;
import com.jchaaban.productservice.rest.exception.ProductNotFoundException;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductFetchingQueryHandler {
    private final ProductRepository productRepository;

    public ProductFetchingQueryHandler(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @QueryHandler
    public List<ReadProductDto> fetchProducts(FetchProductsQuery getProductsQuery) {
        ArrayList<ReadProductDto> products = new ArrayList<>();
        List<ProductEntity> storedProducts = productRepository.findAll();

        storedProducts.forEach(
                productEntity -> products.add(new ReadProductDto(
                        productEntity.getProductId(),
                        productEntity.getTitle(),
                        productEntity.getPrice(),
                        productEntity.getQuantity()
                ))
        );
        return products;
    }

    @QueryHandler
    public ReadProductDto fetchProduct(FetchProductQuery fetchProductQuery) {
        String productId = fetchProductQuery.getProductId();
        ProductEntity productEntity = productRepository.findByProductId(productId);

        if (productEntity == null) {
            throw new ProductNotFoundException("The product with the ID " + productId + " doesn't exist");
        }

        return new ReadProductDto(
                productEntity.getProductId(),
                productEntity.getTitle(),
                productEntity.getPrice(),
                productEntity.getQuantity()
        );
    }

}
