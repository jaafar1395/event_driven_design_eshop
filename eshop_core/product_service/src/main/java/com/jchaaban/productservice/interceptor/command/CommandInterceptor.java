package com.jchaaban.productservice.interceptor.command;

import com.jchaaban.productservice.command.ProductCommand;
import com.jchaaban.productservice.command.validator.ProductCommandValidator;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.BiFunction;

@Component
public class CommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

    private final ProductCommandValidator productCommandValidator;

    public CommandInterceptor(ProductCommandValidator productCommandValidator) {
        this.productCommandValidator = productCommandValidator;
    }

    @Nonnull
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(
            @Nonnull List<? extends CommandMessage<?>> list
    ) {
        return (index, command) -> {
            productCommandValidator.validate((ProductCommand) command.getPayload());
            return command;
        };
    }


}
