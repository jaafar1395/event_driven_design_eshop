package com.jchaaban.productservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, String>{

    ProductEntity findByProductId(String productId);
    ProductEntity findByTitle(String title);
}
