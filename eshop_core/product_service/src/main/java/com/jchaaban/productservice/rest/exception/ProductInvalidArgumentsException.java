package com.jchaaban.productservice.rest.exception;

public class ProductInvalidArgumentsException extends ProductServiceException {
    public ProductInvalidArgumentsException(String message) {
        super(message);
    }
}
