package com.jchaaban.productservice.rest.exception;

import lombok.Data;

@Data
public class ProductServiceErrorResponse {
    private final String errorMessage;

}
