package com.example.shippingservice.handler.command;

import com.jchaaban.common.event.ShipmentCreatedEvent;
import com.example.shippingservice.repository.AddressEntity;
import com.example.shippingservice.repository.ShippingEntity;
import com.example.shippingservice.repository.ShippingRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ShipmentEventHandler {

    private final ShippingRepository shippingRepository;

    public ShipmentEventHandler(ShippingRepository shippingRepository) {
        this.shippingRepository = shippingRepository;
    }

    @EventHandler
    public void on(ShipmentCreatedEvent shipmentCreatedEvent) {


        AddressEntity address = new AddressEntity(
                UUID.randomUUID().toString(),
                shipmentCreatedEvent.getAddress().getStreetAddress(),
                shipmentCreatedEvent.getAddress().getCity(),
                shipmentCreatedEvent.getAddress().getState(),
                shipmentCreatedEvent.getAddress().getPostalCode(),
                shipmentCreatedEvent.getAddress().getCountry()
        );


        ShippingEntity paymentDetailsEntity = new ShippingEntity(
                shipmentCreatedEvent.getShipmentId(),
                shipmentCreatedEvent.getOrderId(),
                address,
                shipmentCreatedEvent.getShippingStatus(),
                shipmentCreatedEvent.getTrackingNumber(),
                shipmentCreatedEvent.getTotalPrice()
        );

        shippingRepository.save(paymentDetailsEntity);
    }
}
