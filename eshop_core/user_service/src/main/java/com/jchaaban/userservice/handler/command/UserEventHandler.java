package com.jchaaban.userservice.handler.command;

import com.jchaaban.userservice.event.UserCreatedEvent;
import com.jchaaban.userservice.event.UserDeletedEvent;
import com.jchaaban.userservice.event.UserUpdatedEvent;
import com.jchaaban.userservice.repository.AddressEntity;
import com.jchaaban.userservice.repository.UserEntity;
import com.jchaaban.userservice.repository.UserRepository;
import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.logging.Logger;

@Component
public class UserEventHandler {

    private final UserRepository userRepository;

    public UserEventHandler(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private static final Logger logger = Logger.getLogger(UserEventHandler.class.getName());

    @EventHandler
    public void on(UserCreatedEvent userCreatedEvent){
        logger.info("We are in UserEventHandler on " + userCreatedEvent.getClass().getName());
        AddressEntity address = new AddressEntity(
                UUID.randomUUID().toString(),
                userCreatedEvent.getAddress().getStreetAddress(),
                userCreatedEvent.getAddress().getCity(),
                userCreatedEvent.getAddress().getState(),
                userCreatedEvent.getAddress().getPostalCode(),
                userCreatedEvent.getAddress().getCountry()
        );


        UserEntity user = new UserEntity(
                userCreatedEvent.getUserId(),
                userCreatedEvent.getFirstname(),
                userCreatedEvent.getLastname(),
                address
        );

        user.setAddress(address);

        userRepository.save(user);
    }

    @EventHandler
    public void on(UserUpdatedEvent userUpdatedEvent) {
        logger.info("We are in UserEventHandler on " + userUpdatedEvent.getClass().getName());
        AddressEntity newAddress = new AddressEntity(
                UUID.randomUUID().toString(),
                userUpdatedEvent.getAddress().getStreetAddress(),
                userUpdatedEvent.getAddress().getCity(),
                userUpdatedEvent.getAddress().getState(),
                userUpdatedEvent.getAddress().getPostalCode(),
                userUpdatedEvent.getAddress().getCountry()
        );


        userRepository.findById(userUpdatedEvent.getUserId()).ifPresent(existingUserEntity -> {
            existingUserEntity.setFirstname(userUpdatedEvent.getFirstname());
            existingUserEntity.setLastname(userUpdatedEvent.getLastname());
            existingUserEntity.setAddress(newAddress);
            userRepository.save(existingUserEntity);
        });
    }

    @EventHandler
    public void on(UserDeletedEvent userDeletedEvent) {
        userRepository.findById(userDeletedEvent.getUserId()).ifPresent(userRepository::delete);
    }

}
