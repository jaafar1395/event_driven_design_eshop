package com.jchaaban.userservice.intercepter.command;

import com.jchaaban.userservice.command.UserCommand;
import com.jchaaban.userservice.command.validator.UserCommandValidator;
import org.axonframework.commandhandling.CommandMessage;
import org.axonframework.messaging.MessageDispatchInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.function.BiFunction;

@Component
public class CommandInterceptor implements MessageDispatchInterceptor<CommandMessage<?>> {

    private final UserCommandValidator userCommandValidator;

    public CommandInterceptor(UserCommandValidator userCommandValidator) {
        this.userCommandValidator = userCommandValidator;
    }

    @Nonnull
    @Override
    public BiFunction<Integer, CommandMessage<?>, CommandMessage<?>> handle(
            @Nonnull List<? extends CommandMessage<?>> list
    ) {
        return (index, command) -> {
            userCommandValidator.validate((UserCommand) command.getPayload());
            return command;
        };
    }


}
