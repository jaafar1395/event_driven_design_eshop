package com.jchaaban.userservice.rest.exception;

public class DuplicatedFullNameException extends UserServiceException {
    public DuplicatedFullNameException(String message) {
        super(message);
    }
}
