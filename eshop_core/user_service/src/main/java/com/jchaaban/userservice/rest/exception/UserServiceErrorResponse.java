package com.jchaaban.userservice.rest.exception;

import lombok.Data;

@Data
public class UserServiceErrorResponse {
    private final String errorMessage;
}
